# DEVOPS WEBINAR CLOUD - 2024

![Markdown](https://img.shields.io/badge/markdown-%23000000.svg?style=for-the-badge&logo=markdown&logoColor=white)
![Shell Script](https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white)
![GitLab CI](https://img.shields.io/badge/gitlab%20CI-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)
![Ansible](https://img.shields.io/badge/ansible-%231A1918.svg?style=for-the-badge&logo=ansible&logoColor=white)
![](https://img.shields.io/badge/version-0,1-red)

![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)
![Vagrant](https://img.shields.io/badge/vagrant-%231563FF.svg?style=for-the-badge&logo=vagrant&logoColor=white)
![Terraform](https://img.shields.io/badge/terraform-%235835CC.svg?style=for-the-badge&logo=terraform&logoColor=white)
![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)

![Fedora](https://img.shields.io/badge/Fedora-294172?style=for-the-badge&logo=fedora&logoColor=white)
![Debian](https://img.shields.io/badge/Debian-D70A53?style=for-the-badge&logo=debian&logoColor=white)
![Ubuntu](https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white)
![DigitalOcean](https://img.shields.io/badge/DigitalOcean-%230167ff.svg?style=for-the-badge&logo=digitalOcean&logoColor=white)

![YAML](https://img.shields.io/badge/yaml-%23ffffff.svg?style=for-the-badge&logo=yaml&logoColor=151515)
![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)
![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white)
![Vim](https://img.shields.io/badge/VIM-%2311AB00.svg?style=for-the-badge&logo=vim&logoColor=white)

![Operating System](https://img.shields.io/badge/platform-linux-success.svg)

## Comandos DOCTL

Podemos obtener datos para las variables Terraform con los siguientes comandos:

```bash
$ doctl compute droplet list
$ doctl compute tag list
$ doctl compute region list
$ doctl compute ssh-key list
$ doctl compute image list --public | grep -i 'fedora'
$ doctl compute size list
```

## Comandos Terraform

```bash
$ terraform init
$ terraform plan -var-file=/path/webinar-iquattro/do-token.tfvars
$ terraform apply -var-file=/path/webinar-iquattro/do-token.tfvars
$ terraform destroy -var-file=/path/webinar-iquattro/do-token.tfvars
```

## Referencias

- [Droplet Details](https://docs.digitalocean.com/products/droplets/)
- [Apuntes DOCTL](https://blog.fcch.xyz/post/notes/notes-xfce4/)
