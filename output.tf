output "droplet_region" {
  value = digitalocean_droplet.droplet.region
}

output "droplet_name" {
  value = digitalocean_droplet.droplet.name
}

output "droplet_ip_address" {
  value = digitalocean_droplet.droplet.ipv4_address
}
